# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

stages:
  - check
  - test
  - package-build
  - package-publish

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  POETRY_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pypoetry"
  # Speedup caching of node modules
  # see https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2684
  FF_USE_FASTZIP: 1
  CACHE_COMPRESSION_LEVEL: "fastest"

default:
  image: python:3.11-bookworm
  interruptible: true
  cache:
    key:
      files:
        - poetry.lock
        - package-lock.json
    paths:
      - .cache/pip
      - .cache/pypoetry
      - .cache/npm
  before_script:
    - "pip install pipx"
    - "pipx ensurepath"
    - "pipx install poetry==1.3.2"
    - "python -m venv .venv"
    - "/root/.local/bin/poetry config http-basic.peren_package_registry -- gitlab-ci-token ${CI_JOB_TOKEN}"
    - "/root/.local/bin/poetry install --without=systemd"
    - "if test -f package.json ; then curl -fsSL https://deb.nodesource.com/setup_16.x | bash - && apt-get install -y nodejs ; fi"

.check:
  stage: check
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs: []

licensing:
  extends: .check
  script:
    - ".venv/bin/licensecheck"
    - ".venv/bin/reuse lint"

linting:
  extends: .check
  script:
    - "./.venv/bin/flake8 src/"
    # flake8-nb could be used to lint python files as well
    # but we split checks for notebooks linting to have dedicated options
    - "find . -name '*.ipynb' -exec flake8_nb {} \\;"
    - "./.venv/bin/pylint --output-format=colorized src/ tests/ -d R,C || ./.venv/bin/pylint-exit $?"
    - "./.venv/bin/black --check src/"


.test:
  stage: test
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs: []

test:
  extends: .test
  script:
    - "./.venv/bin/pytest --cov=tchap_bot --junitxml=report.xml tests/"
    - "./.venv/bin/coverage xml"
    - "./.venv/bin/coverage html"

  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    paths:
      - htmlcov
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

package-build:
  stage: package-build
  script:
    - /root/.local/bin/poetry build
  artifacts:
    paths:
      - "dist/"

package-publish:
  stage: package-publish
  rules:
     - if: $CI_COMMIT_BRANCH == "main"
  script:
     - /root/.local/bin/poetry config repositories.gitlab "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi"
     - /root/.local/bin/poetry config http-basic.gitlab -- gitlab-ci-token "$CI_JOB_TOKEN"
     - /root/.local/bin/poetry publish --repository gitlab

package-publish-central:
  stage: package-publish
  script:
     - /root/.local/bin/poetry config repositories.gitlab-central "${CI_API_V4_URL}/projects/${CENTRAL_REGISTRY_ID}/packages/pypi"
     - /root/.local/bin/poetry config http-basic.gitlab-central -- gitlab-ci-token "$CI_JOB_TOKEN"
     - /root/.local/bin/poetry publish --repository gitlab-central
  rules:
     - if: $CI_COMMIT_BRANCH == "main" && $CENTRAL_REGISTRY_ID

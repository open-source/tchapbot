# SPDX-FileCopyrightText: 2021 - 2022 Isaac Beverly <https://github.com/imbev>
# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import datetime

from nio import MatrixRoom, Event

from matrix_bot.bot import MatrixBot
from matrix_bot.client import MatrixClient
from matrix_bot.callbacks import properly_fail
from matrix_bot.eventparser import MessageEventParser, ignore_when_not_concerned

from tchap_bot.config import env_config


# le décorateur @properly_fail va permettre à la commande de laisser un message d'erreur si la commande plante et
# d'envoyer le message que le bot n'est plus en train d'écrire
# la fonction va être appelée dans tous les cas, le décorateur @ignore_when_not_concerned
# permet de laisser event_parser gérer le cas où la commande n'est pas concernée
@properly_fail
@ignore_when_not_concerned
async def heure(room: MatrixRoom, message: Event, matrix_client: MatrixClient):
    # on initialise un event_parser pour décider à quel message cette commande va répondre
    event_parser = MessageEventParser(room=room, event=message, matrix_client=matrix_client)
    # il ne va pas répondre à ses propres messages
    event_parser.do_not_accept_own_message()
    # il ne va répondre qu'au message "!heure"
    event_parser.command("heure")
    heure = f"il est {datetime.datetime.now().strftime('%Hh%M')}"
    # il envoie l'information qu'il est en train d'écrire
    await matrix_client.room_typing(room.room_id)
    # il envoie le message
    await matrix_client.send_text_message(room.room_id, heure)


if __name__ == "__main__":
    # usage .venv/bin/python examples/bot_horaire.py
    tchap_bot = MatrixBot(
        env_config.matrix_home_server,
        env_config.matrix_bot_username,
        env_config.matrix_bot_password,
        use_functions=True,
    )
    tchap_bot.callbacks.register_on_message_event(heure)
    tchap_bot.run()

# SPDX-FileCopyrightText: 2021 - 2022 Isaac Beverly <https://github.com/imbev>
# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT
import asyncio
import sys
from typing import Optional

from tchap_bot.config import env_config
from tchap_bot.bot import MatrixBot


async def send_direct_message(
    matrix_bot: MatrixBot, message: str, user_to_invite: str, room_name: Optional[str] = None
):
    await matrix_bot.connect()
    room_creation = await matrix_bot.matrix_client.room_create(name=room_name, is_direct=True, invite=[user_to_invite])
    await matrix_bot.matrix_client.sync()
    await matrix_bot.matrix_client.send_text_message(room_creation.room_id, message)


def main():
    """
    usage
    .venv/bin/python examples/send_message_to_given_username.py
    '@me.myself-finances.gouv.fr:agent.finances.tchap.gouv.fr' "Bonjour"
    """
    user, message = sys.argv[1], sys.argv[2]
    room_name = sys.argv[3] if len(sys.argv) > 3 else None
    matrix_bot = MatrixBot(
        env_config.matrix_home_server,
        env_config.matrix_bot_username,
        env_config.matrix_bot_password,
        use_functions=True,
    )
    asyncio.run(send_direct_message(matrix_bot, message, user, room_name))


if __name__ == "__main__":
    main()
